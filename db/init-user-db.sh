#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE ROLE handy WITH LOGIN;
    CREATE DATABASE handy;
    GRANT ALL PRIVILEGES ON DATABASE handy TO handy;
EOSQL
