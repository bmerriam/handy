#!/usr/bin/python3

from os import environ, path
from dotenv import load_dotenv
from coinbase.wallet.client import Client
from tabulate import tabulate

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

API_KEY = environ.get('API_KEY')
API_SECRET = environ.get('API_SECRET')

client = Client(API_KEY, API_SECRET, api_version='2016-03-08')

def _get_paginated_items(api_method, limit=100):
    """Generic getter for paginated items"""
    all_items = []
    starting_after = None
    while True:
        items = api_method(limit=limit, starting_after=starting_after)
        if items.pagination.next_starting_after is not None:
            starting_after = items.pagination.next_starting_after
            all_items += items.data
        else:
            all_items += items.data
            break
    return all_items


def get_accounts(client, limit=300):
    return _get_paginated_items(client.get_accounts, limit)

def get_transactions(account, limit=300):
    return _get_paginated_items(account.get_transactions, limit)

def get_account_totals(account):
    total = 0
    transactions = get_transactions(account)
    for transaction in transactions:
        if transaction['type'] == 'buy':
            total = float(transaction['native_amount']['amount']) + total

    sold = 0
    for transaction in transactions:
        if transaction['type'] == 'cardspend' or transaction['type'] == 'transfer' or transaction['type'] == 'send' or transaction['type'] == 'pro_withdrawal' or transaction['type'] == 'sell':
            sold = float(transaction['native_amount']['amount']) + sold
    return total, sold



accounts = get_accounts(client)
table_data = []
count = 0
for account in accounts:
    #print(account)
    bal =  float(account['native_balance']['amount'])
    
    if bal > 0.0 or account['currency'] == "ETH":
        buy_total, sold = get_account_totals(account)
        table_data.append([account['currency'], bal, buy_total, sold, (bal - buy_total) - sold])
        #break
        count = count + 1
      #  if count == 2:
      #       break
#print(table_data)
print(tabulate(table_data, headers=['Name', 'Balance', 'USD Bought', 'USD Sold', 'Profit']))
print(sum(row[4] for row in table_data)) 
#        for transaction in transactions:
            #print(transaction)
#            break
#        client.get_transactions(account['id'])
#        break

import sys
sys.exit()



sold = 0
for transaction in account.get_transactions()['data']:
    #print(transaction['type'])
    if transaction['type'] == 'cardspend':
        sold = float(str(transaction['native_amount']).split(" ")[1]) + sold
  #  break

print("bought:" + str(total) + " sold:" + str(sold) + " CurrentSpent:" + str((sold + total)))



print("balance: " + str(current))
print("profit: " + str(current - (sold + total)))