import sqlalchemy

from db.config import metadata, engine

accounts = sqlalchemy.Table(
    "accounts",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("account_id", sqlalchemy.String),    
    sqlalchemy.Column("type", sqlalchemy.String),
    sqlalchemy.Column("currency", sqlalchemy.String),
    sqlalchemy.Column("balance", sqlalchemy.String),
    sqlalchemy.Column("native_amount", sqlalchemy.String),
    sqlalchemy.Column("native_currency", sqlalchemy.String)
)

metadata.create_all(bind=engine)
