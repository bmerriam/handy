import sqlalchemy

from db.config import metadata, engine

transactions = sqlalchemy.Table(
    "transactions",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("type", sqlalchemy.String),
    sqlalchemy.Column("amount", sqlalchemy.String),
    sqlalchemy.Column("currency", sqlalchemy.String),
    sqlalchemy.Column("native_amount", sqlalchemy.String),
    sqlalchemy.Column("native_currency", sqlalchemy.String)
)

metadata.create_all(bind=engine)
