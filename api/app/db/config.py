import os
import sys
import databases
import sqlalchemy


from os import environ, path
from dotenv import load_dotenv

# Load configuration values from the .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

DATABASE_URL = environ.get("SQLALCHEMY_DATABASE_URI")
database = databases.Database(DATABASE_URL)

print(DATABASE_URL)

# Create the database engine
engine = sqlalchemy.create_engine(DATABASE_URL)

# Generate the table schema
metadata = sqlalchemy.MetaData()
