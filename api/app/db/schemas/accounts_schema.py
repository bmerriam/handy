from datetime import date
from pydantic import BaseModel


class Accounts(BaseModel):
    account_id: str
    type: str
    currency: str
    balance: str
    native_amount: str
    native_currency: str

    class Conifg:
        orm_mode = True
