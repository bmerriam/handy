from datetime import date
from pydantic import BaseModel


class Transactions(BaseModel):
    id: str
    type: str
    status: str
    amount: str
    currency: str
    native_amount: str
    native_currency: str
    description: str
    created_at: date
    updated_at: date
    resource: str
    resource_path: str
    details_title: str
    details_subtitle: str
    
    class Conifg:
        orm_mode = True
