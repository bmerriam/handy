from fastapi import FastAPI
from fastapi_crudrouter import DatabasesCRUDRouter

from db.config import database
from db.schemas.transactions_schema import Transactions
from db.tables.transactions_table import transactions

from db.schemas.accounts_schema import Accounts
from db.tables.accounts_table import accounts

description = """
An item backend for handy
"""

app = FastAPI(root_path="", title="handy", description=description, version="0.0.1")


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


transactions = DatabasesCRUDRouter(schema=Transactions, table=transactions, database=database)
accounts = DatabasesCRUDRouter(schema=Accounts, table=accounts, database=database)

app.include_router(transactions)
app.include_router(accounts)
