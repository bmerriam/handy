﻿(function ($) {
  $.fn.extend({
    vscroller: function (options) {
      var settings = $.extend(
        { speed: 1000, stay: 2000, newsfeed: "", cache: false },
        options
      );

      return this.each(function () {
        var interval = null;
        var mouseIn = false;
        var totalElements;
        var isScrolling = false;
        var h;
        var t;
        var wrapper = $(this).addClass("news-wrapper");

        if (settings.newsfeed == "") {
          alert("No URL specified");
          return;
        }

        $.ajax({
          url: settings.newsfeed,
          type: "GET",
          dataType: "json",
          cache: settings.cache,
          success: function (json) {
            var contentWrapper = $("<div/>").addClass("news-contents-wrapper");
            var newsContents = $("<div/>").addClass("news-contents");
            wrapper.append(contentWrapper);
            contentWrapper.append(newsContents);
            var i = 0;
            totalElements = json.length;
            $(json).each(function () {
              var news = $("<div/>").addClass("news").addClass("left");
              //var news = getAlign();
              newsContents.append(news);
              var history = $("<div/>").addClass("history");
              var description = $("<div/>").addClass("description");
              news.append(history);
              news.append(description);
              history.append(getCircle($(this).attr("event_type")));

              var url = this["url"];
              var htext = this["headline"];
              description.append(
                $("<h1/>").html("<a href='" + url + "'>" + htext + "</a>")
              );
              var newsText = this["detail"];
              if (newsText.length > 120) {
                newsText = newsText.substr(0, 120) + "...";
              }
              description.append($("<div/>").addClass("detail").html(newsText));
            });

            h = parseFloat($(".news:eq(0)").outerHeight());
            $(".news", wrapper).each(function () {
              $(this).css({ top: i++ * h });
            });
            t = (totalElements - 1) * h;
            newsContents.mouseenter(function () {
              mouseIn = true;
              if (!isScrolling) {
                $(".news").stop(true, false);
                clearTimeout(interval);
              }
            });
            newsContents.mouseleave(function () {
              mouseIn = false;
              interval = setTimeout(scroll, settings.stay);
            });
            interval = setTimeout(scroll, 1);
          },
        });
        // function getAlign() {
        //   var aligns = new Array();
        //   aligns[0] = "left";
        //   aligns[1] = "right";
        //   aligns[2] = "middle";
          
        //   var align = aligns[Math.floor(Math.random()*3)];

        //   return  $("<div/>")
        //            .addClass("news")
        //            .addClass(align);
        // }

        function scroll() {
          if (!mouseIn && !isScrolling) {
            isScrolling = true;
            $(".news:eq(0)")
              .stop(true, false)
              .animate({ top: -h }, settings.speed, function () {
                clearTimeout(interval);
                var current = $(".news:eq(0)").clone(true);
                current.css({ top: t });
                $(".news-contents").append(current);
                $(".news:eq(0)").remove();
                isScrolling = false;
                interval = setTimeout(scroll, settings.stay);
              });
            $(".news:gt(0)")
              .stop(true, false)
              .animate({ top: "-=" + h }, settings.speed);
          }
        }
        
        function getCircle(category) {
          return $("<div/>")
            .addClass("circle-outer")
            .append(
              $("<div/>")
                .addClass("circle")
                .addClass(category)
                .append($("<span/>").html("...").addClass("elipses"))
                .append($("<span/>").html(category).addClass("elipses"))
            );
        }
      });
    },
  });
})(jQuery);
