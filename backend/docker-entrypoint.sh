#!/bin/sh
set -e

if [ $(echo "$1" | cut -c1) = "-" ]; then
  echo "$0: assuming arguments for dravited"

  set -- dravited "$@"
fi

if [ $(echo "$1" | cut -c1) = "-" ] || [ "$1" = "dravited" ]; then
  mkdir -p "$COIN_DATA"
  chmod 700 "$COIN_DATA"
  chown -R dravite "$COIN_DATA"

  echo "$0: setting data directory to $COIN_DATA"

  set -- "$@" -datadir="$COIN_DATA"
fi

if [ "$1" = "dravited" ]; then
  echo
  exec gosu dravite "$@"
fi

echo
exec "$@"
