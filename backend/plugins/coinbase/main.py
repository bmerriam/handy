"""Random Weather

Returns:
    string: City Name and current weather
"""

import requests
import json
import logging


from os import environ, path
from dotenv import load_dotenv

from coinbase.wallet.client import Client

# Load configuration values from the .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

API_KEY = environ.get("API_KEY")
API_SECRET = environ.get("API_SECRET")


class CB:
    def __init__(self):
        self.logger = logging.getLogger("handy")
        self.API_ENDPOINT = "http://127.0.0.1:5050"
        self.client = Client(API_KEY, API_SECRET, api_version="2016-03-08")

    def get_cb_paginated_items(self, api_method, limit=100):
        """Generic getter for paginated items"""
        all_items = []
        starting_after = None
        while True:
            items = api_method(limit=limit, starting_after=starting_after)
            if items.pagination.next_starting_after is not None:
                starting_after = items.pagination.next_starting_after
                all_items += items.data
            else:
                all_items += items.data
                break
        return all_items

    def get_cb_accounts(self, client, limit=300):
        accounts = self.get_cb_paginated_items(client.get_accounts, limit)
        return accounts

    def get_cb_transactions(self, account_id):
        return self.client.get_transactions(account_id)

    def get_api_accounts(self):

        try:
            response = requests.get(f"{self.API_ENDPOINT}/accounts")
        except Exception as e:
            self.logger.error(f"Unable to get accounts : {e}")
        return response.json()

    def update_cb_accounts_in_api(self):

        cb_accounts = self.get_cb_accounts(self.client)
        api_accounts = self.get_api_accounts()

        api_ids = []
        cb_ids = []

        for api_account in api_accounts:
            api_ids.append(api_account["account_id"])

        for cb_account in cb_accounts:
            if cb_account["id"] not in api_ids:

                data = {
                    "account_id": cb_account["id"],
                    "type": "coinbase",
                    "currency": cb_account["currency"],
                    "balance": cb_account["balance"]["amount"],
                    "native_amount": cb_account["native_balance"]["amount"],
                    "native_currency": cb_account["native_balance"]["currency"],
                }

                self.publish("accounts", data).json()

        self.logger.info("Accounts Updated")

    def update_cb_transactions_in_api(self):
        api_accounts = self.get_api_accounts()
        for api_account in api_accounts:
            transactions = self.get_cb_transactions(api_account["account_id"])
            print(transactions)
            if transactions == {"data": []}:
                continue
            break

    def publish(self, resource, data):

        try:
            response = requests.post(
                f"{self.API_ENDPOINT}/{resource}", json=data
            )
        except Exception as e:
            self.logger.error(f"Unable to publish data: {resource} : {data} : {e}")

        return response

    def run(self):

        # Update Accounts
        self.update_cb_accounts_in_api()

        # Update Transactions
        self.update_cb_transactions_in_api()

        return ""
        # return "string"
